package com.example.user.basicproject.network;

import com.example.user.basicproject.model.ServerResponse;

import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public interface ApiInterface {

    Observable<ServerResponse> getForecast(String cityname);
}

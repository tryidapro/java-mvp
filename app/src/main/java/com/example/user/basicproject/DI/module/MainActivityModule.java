package com.example.user.basicproject.DI.module;

import com.example.user.basicproject.DI.scopes.FragmentScope;
import com.example.user.basicproject.UI.base.BaseRootFragment;
import com.example.user.basicproject.UI.first.FragmentOne;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface MainActivityModule {


    @FragmentScope
    @ContributesAndroidInjector(modules = {MainSimpleFragmentModule.class})
    FragmentOne mainSimpleFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract BaseRootFragment baseRootFragment();



}

package com.example.user.basicproject.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ListInfo {
    @JsonProperty("dt")
    private Long dt;

    @JsonProperty("temp")
    private Temp temp;

    @JsonProperty("weather")
    private List<Weather> weatherList;

    public String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy");
        return format.format(new Date(dt));
    }

    public Temp getTemp() {
        if (temp == null)
            return new Temp();
        return temp;
    }

    public List<Weather> getWeather() {
        if (weatherList == null)

            return new ArrayList();
        return weatherList;
    }
}

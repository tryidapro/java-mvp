package com.example.user.basicproject.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ServerResponse {
    @JsonProperty("list")
    List<ListInfo> list;
    @JsonProperty("cod")
    private String cod;
    @JsonProperty("message")
    private String message;
    @JsonProperty("city")
    private City city;


    public List<ListInfo> getListInfo() {
        if (list == null) {
            return new ArrayList<>();
        }
        return list;
    }

    public String getCod() {
        if (cod == null)
            return "null";
        return cod;
    }

    public String getMessage() {
        if (message == null)
            return "null";
        return message;
    }

    public City getCity() {
        if (city == null)
            return new City();
        return city;
    }


}

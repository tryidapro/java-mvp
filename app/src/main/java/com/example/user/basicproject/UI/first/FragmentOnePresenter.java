package com.example.user.basicproject.UI.first;

import com.example.user.basicproject.UI.base.BasePresenterImpl;
import com.example.user.basicproject.model.ResponseHandler;
import com.example.user.basicproject.model.ServerResponse;

import javax.inject.Inject;

public class FragmentOnePresenter extends BasePresenterImpl<FragmentOneEvents.View> implements FragmentOneEvents.Presenter {


    @Inject
    public FragmentOnePresenter() {

    }

    @Override
    public void onLoadData() {
        loadData(new ResponseHandler() {
            @Override
            public void success(ServerResponse response) {
                getView().onInfoLoaded(response.getListInfo());

            }

            @Override
            public void businessError() {
                getView().showError("Server error");
            }


        },apiClient.getForecast("524901"),true, true);
    }


}

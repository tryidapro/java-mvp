package com.example.user.basicproject.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Temp {

    @JsonProperty("day")
    private String day;

    @JsonProperty("min")
    private String min;

    @JsonProperty("max")
    private String max;


    public String getDay() {
        if (day == null)
            return "null";
        return day;
    }

    public String getMin() {
        if (min == null)
            return "null";
        return min;
    }

    public String getMax() {
        if (max == null)
            return "null";
        return max;
    }

}

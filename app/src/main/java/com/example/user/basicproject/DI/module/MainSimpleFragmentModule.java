package com.example.user.basicproject.DI.module;

import com.example.user.basicproject.DI.scopes.FragmentScope;
import com.example.user.basicproject.UI.first.FragmentOneEvents;
import com.example.user.basicproject.UI.first.FragmentOne;
import com.example.user.basicproject.UI.first.FragmentOnePresenter;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class MainSimpleFragmentModule {

    @Binds
    @FragmentScope
    public abstract FragmentOneEvents.Presenter presenter(FragmentOnePresenter presenter);

    @Binds
    public abstract FragmentOneEvents.View getFragment(FragmentOne fragment);

}
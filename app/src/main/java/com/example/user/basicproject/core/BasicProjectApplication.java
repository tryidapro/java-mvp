package com.example.user.basicproject.core;

import android.app.Activity;
import android.support.multidex.MultiDexApplication;

import com.example.user.basicproject.DI.component.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class BasicProjectApplication  extends MultiDexApplication implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent
                .builder()
                .context(this)
                .build()
                .inject(this);
        //LeakCanary.install(this);
    }


}

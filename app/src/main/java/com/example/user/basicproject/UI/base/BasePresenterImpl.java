package com.example.user.basicproject.UI.base;

import com.example.user.basicproject.MVP.interfaces.BasePresenter;
import com.example.user.basicproject.MVP.interfaces.BaseView;
import com.example.user.basicproject.model.ResponseHandler;
import com.example.user.basicproject.model.ServerResponse;
import com.example.user.basicproject.network.ApiInterface;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;


abstract public class BasePresenterImpl<V extends BaseView> implements BasePresenter<V> {

    @Inject
    protected ApiInterface apiClient;
    private V view;
    private CompositeDisposable compositeDisposable;

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView(V view) {
        this.view = null;
    }

    @Override
    public void resume(V view) {
        this.view = view;
    }

    @Override
    public boolean viewIsReady() {
        return view != null;
    }

    @Override
    public V getView() {
        return view;
    }

    @Override
    public void destroy() {
        this.view = null;
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

    protected <T> void loadData(final ResponseHandler handler, Observable<ServerResponse> observable, final boolean showProgress, final boolean showError) {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (viewIsReady()) {
            if (showProgress) {
                getView().showProgress();
            }
        }
        compositeDisposable.add(observable.subscribeWith(new DisposableObserver<ServerResponse>() {
            @Override
            public void onComplete() {
                if (viewIsReady()) {
                    getView().hideProgress();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (viewIsReady()) {
                    if (showError)
                        getView().showError("error");
                    getView().hideProgress();
                }
            }

            @Override
            public void onNext(ServerResponse response) {
                if (viewIsReady()) {
                    if (response.getListInfo().size() > 0) {
                        handler.success(response);
                    } else {
                        handler.businessError();
                    }
                    getView().hideProgress();
                }
            }
        }));

    }


}

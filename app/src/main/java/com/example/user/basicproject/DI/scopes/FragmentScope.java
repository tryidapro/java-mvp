package com.example.user.basicproject.DI.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@FragmentScope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentScope {
}

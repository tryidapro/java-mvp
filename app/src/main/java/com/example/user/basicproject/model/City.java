package com.example.user.basicproject.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class City {
    @JsonProperty("name")
    private String name;

    public String getName() {
        if (name == null)
            return "null";
        return name;
    }


}

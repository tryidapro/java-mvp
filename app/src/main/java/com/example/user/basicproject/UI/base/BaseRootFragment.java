package com.example.user.basicproject.UI.base;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.basicproject.MainActivity;
import com.example.user.basicproject.MainActivityEvents;
import com.example.user.basicproject.R;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;


public class BaseRootFragment extends Fragment{



    @Inject
    MainActivityEvents.Presenter mainPresenter;
    private BaseActivity activity;

    public static BaseRootFragment newInstance(String routeTag) {
        Bundle args = new Bundle();
        BaseRootFragment fragment = new BaseRootFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MainActivity) getActivity();

    }



    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.root_frg, null);
    }


}

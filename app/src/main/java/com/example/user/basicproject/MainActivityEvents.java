package com.example.user.basicproject;


import com.example.user.basicproject.MVP.interfaces.BasePresenter;
import com.example.user.basicproject.MVP.interfaces.BaseView;

import java.util.Deque;

public interface MainActivityEvents {

    interface View extends BaseView {
        void addSecondFragment(String day);
    }

    interface Presenter extends BasePresenter<View> {

       void openCard(String day);

    }
}

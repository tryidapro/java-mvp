package com.example.user.basicproject;


import android.os.Bundle;

import com.example.user.basicproject.UI.base.BaseActivity;
import com.example.user.basicproject.UI.first.FragmentOne;
import com.example.user.basicproject.UI.second.FragmentTwo;

import javax.inject.Inject;


public class MainActivity extends BaseActivity implements MainActivityEvents.View {


    @Inject
    MainActivityEvents.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        presenter.attachView(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentOne())
                .commit();

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            getSupportFragmentManager()
                    .popBackStackImmediate();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1)
            getSupportFragmentManager()
                    .popBackStackImmediate();
        else super.onBackPressed();

    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView(this);
    }

    @Override
    public void addSecondFragment(String day) {
        Bundle bundle = new Bundle();
        bundle.putString("info", day);
        FragmentTwo fragmentTwo = new FragmentTwo();
        fragmentTwo.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .add(R.id.content_frame, fragmentTwo)
                .addToBackStack(null)
                .commit();

    }
}

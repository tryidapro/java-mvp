package com.example.user.basicproject.UI.second;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.basicproject.R;

public class FragmentTwo extends Fragment {



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_simple_fragment, null);
        TextView textView = (TextView) view.findViewById(R.id.textView);
        if (this.getArguments() != null) {
            String info = this.getArguments().getString("info");
            textView.setText("fragment two:" + info);
        }
        return view;

    }
}

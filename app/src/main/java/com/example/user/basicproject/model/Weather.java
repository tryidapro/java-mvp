package com.example.user.basicproject.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Weather {

    @JsonProperty("main")
    private String main;

    public String getMain() {
        if (main == null)
            return "null";
        return main;
    }
}

package com.example.user.basicproject.DI.module;


import com.example.user.basicproject.DI.scopes.ActivityScope;
import com.example.user.basicproject.MainActivity;
import com.example.user.basicproject.MainActivityEvents;
import com.example.user.basicproject.MainActivityPresenter;
import com.example.user.basicproject.network.ApiData;
import com.example.user.basicproject.network.ApiInterface;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = {AndroidSupportInjectionModule.class})
public abstract class AppModule {


    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity mainActivityInjector();


    @Provides
    @Singleton
    public static ApiInterface apiInterface() {
        return new ApiData();
    }


    @Provides
    @Singleton
    public static MainActivityEvents.Presenter presenter() {
        return new MainActivityPresenter();
    }


}

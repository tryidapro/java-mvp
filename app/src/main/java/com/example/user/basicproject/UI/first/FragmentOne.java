package com.example.user.basicproject.UI.first;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.basicproject.R;
import com.example.user.basicproject.UI.base.BaseFragmentWithPresenter;
import com.example.user.basicproject.adapters.WeatherAdapter;
import com.example.user.basicproject.model.ListInfo;

import java.util.List;

public class FragmentOne extends BaseFragmentWithPresenter<FragmentOnePresenter> implements FragmentOneEvents.View {

    View view;
    WeatherAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.main_recycler, null);
        presenter.attachView(this);
        presenter.onLoadData();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new WeatherAdapter();
        adapter.setOnClickListener(new WeatherAdapter.ClickEvent() {
            @Override
            public void onItemClick(String date) {
                parentPresenter.openCard(date);
            }
        });
    }

    @Override
    public void onInfoLoaded(List<ListInfo> info) {
        if (adapter != null) {
            RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
            adapter.setWeatherInfo(getContext(), info);
            LinearLayoutManager manager = new LinearLayoutManager(getContext());
            rv.setLayoutManager(manager);
            rv.setAdapter(adapter);
        }

    }

    @Override
    public void onDestroyView() {
        final RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                // no-op
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                rv.setAdapter(null);
            }
        });
        adapter = null;
        super.onDestroyView();
    }

}


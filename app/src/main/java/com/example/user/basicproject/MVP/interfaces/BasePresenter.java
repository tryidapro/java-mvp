package com.example.user.basicproject.MVP.interfaces;

public interface BasePresenter<V extends BaseView> {

    void attachView(V view);

    void detachView(V view);

    void resume(V view);

    void destroy();

    boolean viewIsReady();

    BaseView getView();

}

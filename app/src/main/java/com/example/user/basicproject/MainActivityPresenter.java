package com.example.user.basicproject;


import android.util.Log;

import com.example.user.basicproject.UI.base.BasePresenterImpl;
import com.example.user.basicproject.UI.base.BaseRootFragment;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class MainActivityPresenter extends BasePresenterImpl<MainActivityEvents.View> implements MainActivityEvents.Presenter {


    @Inject
    public MainActivityPresenter() {

    }


    @Override
    public void openCard(String day) {
        getView().addSecondFragment(day);
    }
}

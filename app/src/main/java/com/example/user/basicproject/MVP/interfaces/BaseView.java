package com.example.user.basicproject.MVP.interfaces;

public interface BaseView {

    void showError(String error);

    void showProgress();

    void hideProgress();

}

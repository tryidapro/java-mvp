package com.example.user.basicproject.model;

public interface ResponseHandler {

    void success(ServerResponse response);

    void businessError();
}

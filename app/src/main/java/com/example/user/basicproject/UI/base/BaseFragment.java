package com.example.user.basicproject.UI.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.example.user.basicproject.MVP.interfaces.BaseView;

abstract public class BaseFragment extends Fragment implements BaseView {


    protected BaseActivity activity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void showError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }





}

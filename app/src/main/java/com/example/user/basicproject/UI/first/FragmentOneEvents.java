package com.example.user.basicproject.UI.first;

import com.example.user.basicproject.MVP.interfaces.BasePresenter;
import com.example.user.basicproject.MVP.interfaces.BaseView;
import com.example.user.basicproject.model.ListInfo;

import java.util.List;

public interface FragmentOneEvents {
    interface View extends BaseView {
        void onInfoLoaded(List<ListInfo> info);
    }

    interface Presenter extends BasePresenter<View> {
        void onLoadData();
    }
}

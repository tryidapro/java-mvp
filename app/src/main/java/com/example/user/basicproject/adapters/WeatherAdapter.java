package com.example.user.basicproject.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.basicproject.R;
import com.example.user.basicproject.model.ListInfo;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ClickEvent listener;

    private Context context;
    private List<ListInfo> data;


    public WeatherAdapter() {

    }

    public void setWeatherInfo(Context context, List<ListInfo> info) {
        this.context = context;
        this.data = info;
    }

    public void setOnClickListener(ClickEvent listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WeatherViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        WeatherViewHolder viewHolder = (WeatherViewHolder) holder;
        viewHolder.bind(position);
    }

    private ListInfo getItem(int position) {
        if (data != null) {
            return data.get(position);
        }
        return null;
    }

    public interface ClickEvent {
        void onItemClick(String date);

    }

    class WeatherViewHolder extends RecyclerView.ViewHolder {
        TextView date;
        TextView day;
        TextView min;
        TextView max;
        TextView weatherField;

        public WeatherViewHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            day = itemView.findViewById(R.id.day);
            min = itemView.findViewById(R.id.min);
            max = itemView.findViewById(R.id.max);
            weatherField = itemView.findViewById(R.id.weather);
        }

        public void bind(final int position) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(data.get(position).getDate());
                    }
                }
            });
            ListInfo info = getItem(position);
            date.setText(String.format(context.getString(R.string.date), info.getDate()));
            day.setText(String.format(context.getString(R.string.day), info.getTemp().getDay()));
            min.setText(String.format(context.getString(R.string.min), info.getTemp().getMin()));
            max.setText(String.format(context.getString(R.string.max), info.getTemp().getMax()));
            if (info.getWeather().size() > 0) {
                weatherField.setText(String.format(context.getString(R.string.weather), info.getWeather().get(0).getMain()));
            }

        }


    }
}


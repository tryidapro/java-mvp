package com.example.user.basicproject.UI.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.user.basicproject.MVP.interfaces.BasePresenter;
import com.example.user.basicproject.MainActivityEvents;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

abstract public class BaseFragmentWithPresenter<P extends BasePresenter> extends BaseFragment {

    @Inject
    protected P presenter;
    private BaseActivity baseActivity;


    @Inject
    public MainActivityEvents.Presenter parentPresenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        this.baseActivity = (BaseActivity) getActivity();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AndroidSupportInjection.inject(this);
        presenter.attachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.detachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume(this);
    }

    @Override
    public void showProgress() {
        baseActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        baseActivity.hideProgress();
    }


}


package com.example.user.basicproject.network;

import com.example.user.basicproject.model.ServerResponse;


import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiMethodInterface {

    @GET("forecast/daily")
    Observable<ServerResponse> getForecast(@QueryMap Map<String, String> params);
}

package com.example.user.basicproject.UI.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.user.basicproject.MVP.interfaces.BaseView;
import com.example.user.basicproject.R;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;


abstract public class BaseActivity extends AppCompatActivity implements BaseView, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }



    public void showProgress() {
        if (findViewById(R.id.progress) != null)
            findViewById(R.id.progress).setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        if (findViewById(R.id.progress) != null)
            findViewById(R.id.progress).setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {

    }
}

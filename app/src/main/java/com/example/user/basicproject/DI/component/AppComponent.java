package com.example.user.basicproject.DI.component;

import android.content.Context;

import com.example.user.basicproject.DI.module.AppModule;
import com.example.user.basicproject.core.BasicProjectApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder context(Context context);

        AppComponent build();
    }

    void inject(BasicProjectApplication app);

}
